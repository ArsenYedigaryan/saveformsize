﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Hello Word!");
            //button2.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Goodbye Word!");
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(this.WindowState ==FormWindowState.Normal)
            {
                Properties.Settings.Default.FormSize = this.Size;
                Properties.Settings.Default.Save();

                    
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if(Properties.Settings.Default.FormSize.Width !=0 && Properties.Settings.Default.FormSize.Height !=0)
            {
                this.Size = Properties.Settings.Default.FormSize;
            }
        }
    }
}
